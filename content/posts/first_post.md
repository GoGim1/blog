---
title: daily life
date: 2021-03-02
---

## 个人博客搭建完成
个人博客搭建完成。感谢Gitlab Pages，感谢hugo，感谢etch。爱心。

晚上的收获是理清了这一系列工具的关系，现记录如下：
- 由于手写html和css来搭建博客挺麻烦，所以hugo就是将markdown博文转成html和css的工具。
- 将hugo的生成产物html打开，就可以看到博客的页面了。
- Gitlab Pages只是将html和css展现出来的一个平台。理论上，将hugo的生成产物上传到gitlab仓库就能显示静态页面，不过找不到相关方法。
- 发表博文，写完markdown还要运行hugo进行转换，最后push。这比较麻烦，所以运行hugo这一步交给gitlab ci。

## 安装zsh
捣鼓了一晚上zsh，记录如下：
- zsh 是终端，ohmyzsh 是一个配置工具
- 我的 zsh 主题选的是 agnoster，它依赖 powerline 字体。在 wsl 环境下，wsl 子系统和 win10 都要安装该字体，同时 window terminal 设置使用它。