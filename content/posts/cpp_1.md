---
title: cpp_1
date: 2021-06-10
---

## 两阶段翻译 Two-Phase Translation
模板会分成两个阶段进行”编译“： 
1. 在不进行模板instantiation的definition time阶段，此时会忽略模板参数，检查如下方面： 
    * 语法错误，包括缺失分号。 
    * 使用未定义参数。 
    * 如果static assertion不依赖模板参数，会检查是否通过static assertion. 
2. 在instantiation阶段，会再次检查模板里所有代码的正确性，尤其是那些依赖模板参数的部分。

## 多个模板函数返回值类型
```
template<typename T1, typename T2>
T1 max (T1 a, T2 b) {
    return b < a ? a : b;
}
// 注意：返回类型总是T1
auto m = max(4, 7.2);       
```
解决方法：
1. 引入额外模板参数作为返回值类型
```
template<typename T1, typename T2, typename RT>
RT max(T1 a, T2 b);
```
当模板参数不能根据传递的参数推导出来时，我们就需要显式的指定模板参数类型。RT是不能根据函数的参数列表推导出来的，所以我们需要显式的指定

2. 让编译器自己找出返回值类型
```
template <typename T1, typename T2>
auto max(T1 a, T2 b) -> decltype(b < a ? a : b) {
  return b < a ? a : b;
}
// 这里的重点不是计算返回值，而是得到返回值类型
template <typename T1, typename T2>
auto max(T1 a, T2 b) -> decltype(true ? a : b) {      
  return b < a ? a : b;
}
// C++14中可以省略trailing return type
template<typename T1, typename T2>
auto max (T1 a, T2 b) {
    return b < a ? a : b;
}
```
3. 将返回值声明为两个模板参数的公共类型
```
template <typename T1, typename T2>
typename std::common_type<T1, T2>::type max(T1 a, T2 b) {
  return b < a ? a : b;
}
// C++14
template <typename T1, typename T2>
std::common_type_t<T1, T2> max(T1 a, T2 b) {     
  return b < a ? a : b;
}
template <typename T1, typename T2, typename RT = std::common_type_t<T1, T2>>
RT max(T1 a, T2 b) {
  return b < a ? a : b;
}
```

## 模板参数类型推导过程中不允许类型自动转换
```
int max(int a, int b) { 
  return b < a ? a : b; 
}
template <typename T> 
T max(T a, T b) { 
  return b < a ? a : b; 
}
// calls the nontemplate for two ints
max('a', 42.7);     
```

## 函数名加括号
避免匹配到宏。WinDef.h 中定义了两个宏 max 和 min，如需要自己定义max、min，需要加括号
```
#define sin(x) __builtin_sin(x)

// parentheses avoid substitution by the macro
double (sin)(double arg) {
    return sin(arg); // uses the macro
}

int main() {
    // uses the macro
    printf("%f\n", sin(3.14));

    // uses the function
    double (*x)(double) = &sin;

    // uses the function
    printf("%f\n", (sin)(3.14));
}
```

## 折叠表达式 Fold Expressions
![](https://pic2.zhimg.com/80/v2-ad4d3da65db2e2984c8b3a718e517cf5_720w.jpg)
```
template <typename FirstType, typename... Args>
void print(FirstType first, Args... args) {
  std::cout << first;

  auto printWhiteSpace = [](const auto arg "") { std::cout << " " << arg; };

  (..., printWhiteSpace(args));
}
```
`(..., printWhiteSpace(args));`会被展开为：`printWhiteSpace(arg1), printWhiteSpace(arg2), printWhiteSpace(arg3)`

## 可变模板参数展开
注意与折叠表达式的区别
```
template<typename... T>
void printDoubled (T const&... args) {
  print (args + args...);
}

printDoubled(7.5, std::string("hello"), std::complex<float>(4,2));
```
上面的调用会展开为：
```
print(7.5 + 7.5,
std::string("hello") + std::string("hello"),
std::complex<float>(4,2) + std::complex<float>(4,2);
```

## Variadic Base Classes and using
```
class Customer {
private:
  std::string name;

public:
  Customer(std::string const &n) : name(n) {}
  std::string getName() const { return name; }
};

struct CustomerEq {
  bool operator()(Customer const &c1, Customer const &c2) const {
    return c1.getName() == c2.getName();
  }
};

struct CustomerHash {
  std::size_t operator()(Customer const &c) const {
    return std::hash<std::string>()(c.getName());
  }
};

// define class that combines operator() for variadic base classes:
template <typename... Bases> struct Overloader : Bases... {
  using Bases::operator()...; // OK since C++17
};

int main() {
  // combine hasher and equality for customers in one type:
  using CustomerOP = Overloader<CustomerHash, CustomerEq>;
  std::unordered_set<Customer, CustomerHash, CustomerEq> coll1;
  std::unordered_set<Customer, CustomerOP, CustomerOP> coll2;
  ...
}
```

## overloaded的实现
```
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

overloaded s{
    [](int){cout << "int" << endl;},
    [](double){cout << "double" << endl;},
    [](string){cout << "string" << endl;},
};
s(1); // int
s(1.); // double
s("1"); // string
```
* `template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };`
这是一个类模板的声明。
* `template<class... Ts>：overloaded` 类的模板参数为可变长的参数包 `Ts`。
假设`Ts`包含 `T1, T2, ... , TN`，那么这一句声明可以展开为：`template<class T1, class T2, ..., class TN> `
* `struct overloaded : Ts...：overloaded` 类的基类为参数包 `Ts` 内所有的参数类型。
假设 `Ts` 包含 `T1, T2, ... , TN`，那么这一句声明可以展开为：`struct overloaded : T1, T2, ..., TN`
* `using Ts::operator()...;`：这是一个变长 using 声明。
假设 `Ts` 包含 `T1, T2, ... , TN`，那么这一句声明可以展开为：`using T1::operator(), T1::operator(), ..., TN::operator();`
也就是说，overloaded 类的基类即参数包 Ts 内所有的参数类型的函数调用操作符均被 overloaded 类引入了自己的作用域。
* `template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;`
这是一个自动推断向导，用于帮助编译器根据 overloaded 构造器参数的类型来推导 overloaded 的模板参数类型。
这个自动推断向导告诉编译器，如果 overloaded 构造器所有参数的类型的集合为Ts，那么 overloaded 的模板参数类型就是 Ts 所包含的所有类型。
也就是说如果表达式 `a1, a2, ..., an` 的类型分别为 `T1, T2, ..., TN`,
那么构造器表达式 `overloaded {a1, a2, ..., an}` 的类型就是 `overloaded<T1, T2, ..., TN>`。
* `overloaded s{
 [](int){cout << "int" << endl;},
 [](double){cout << "double" << endl;},
 [](string){cout << "string" << endl;},
 };`
overloaded 类的实例 s 的构造器包含3个lambda参数，也可以看作3个各自包含一个 operator() 的函数对象。
根据 overloaded 类的定义，s 对象将继承这3个lambda（函数对象）的 operator() ，也就是说这3个lambda的 operator() 即函数体在 s 对象内部形成重载关系。
根据 overloaded 类的自动推断向导，s 对象的类型为`overloaded<T1, T2, T3>`，其中T1, T2, T3为3个lambda参数的类型。
* 通过利用 C++17 的新特性变长的 using 声明以及自动推断向导，overloaded类的实例可以简洁并且巧妙地将多个lambda合成一个大的具有多个相互重载的 operator() 的函数对象。
overloaded 这个类模板如此有用，实现机制又如此精妙，实在是应该早日纳入标准库中。

## 参数推导规则
> https://zhuanlan.zhihu.com/p/338788455
  https://zhuanlan.zhihu.com/p/338798151

##  名称查找与ADL
> https://zhuanlan.zhihu.com/p/338917913

* qualified name : 一个名称所属的作用域被显式的指明，例如::、->或者.。this->count就是一个 qualified name，但 count 不是，因为它的作用域没有被显示的指明，即使它和this->count是等价的。
* dependent name：依赖于模板参数的名称，也就是访问运算符左面的表达式类型依赖于模板参数。例如：`std::vector<T>::iterator` 是一个 Dependent Name，但假如 T 是一个已知类型的别名（using T = int），那就不是 Dependent Name。

对于 qualified name 来说，会有显示指明的作用域。如果作用域是一个类，那么基类也会被考虑在内，但是类的外围作用域不会被考虑。相反，对于非 qualified name 来说，会在外围作用域逐层查找（假如在类成员函数中，会先找本类和基类的作用域）。这叫做 ordinary lookup 

```
template<typename T>
T max (T a, T b) {
    return b < a ? a : b;
}

namespace BigMath {
  class BigNumber {
    ...
};

  bool operator < (BigNumber const&, BigNumber const&);
  ...
}

using BigMath::BigNumber;

void g (BigNumber const& a, BigNumber const& b) {
  ...
  BigNumber x = ::max(a,b);
  ...
}
```
这里的问题是：当调用 max 时，ordinary lookup不会找到 BigNumber 的operator <。如果没有一些特殊规则，那么在 C++ namespace 场景中，会极大的限制模板的适应性。ADL 就是这个特殊规则，用来解决此类的问题。

需要注意的一点是，ADL 会忽略 using:
```
namespace X {
  template <typename T> void f(T);
}

namespace N {
  using namespace X;
  enum E { e1 };
  void f(E) { std::cout << "N::f(N::E) called\n"; }
}    // namespace N

void f(int) { std::cout << "::f(int) called\n"; }

int main() {
  ::f(N::e1);    // qualified function name: no ADL
  f(N::e1);     // ordinary lookup finds ::f() and ADL finds N::f(), the latter is preferred
}
```
```
int main() {
    std::cout << "Test\n"; // There is no operator<< in global namespace, but ADL
                           // examines std namespace because the left argument is in
                           // std and finds std::operator<<(std::ostream&, const char*)
    operator<<(std::cout, "Test\n"); // same, using function call notation

    // however,
    std::cout << endl; // Error: 'endl' is not declared in this namespace.
                       // This is not a function call to endl(), so ADL does not apply

    endl(std::cout); // OK: this is a function call: ADL examines std namespace
                     // because the argument of endl is in std, and finds std::endl

    (endl)(std::cout); // Error: 'endl' is not declared in this namespace.
                       // The sub-expression (endl) is not a function call expression
}
```
注意最后一点(endl)(std::cout);，如果函数的名字被括号包起来了，那也不会应用 ADL。
```
namespace A {
      struct X;
      struct Y;
      void f(int);
      void g(X);
}

namespace B {
    void f(int i) {
        f(i);      // calls B::f (endless recursion)
    }
    void g(A::X x) {
        g(x);   // Error: ambiguous between B::g (ordinary lookup)
                //        and A::g (argument-dependent lookup)
    }
    void h(A::Y y) {
        h(y);   // calls B::h (endless recursion): ADL examines the A namespace
                // but finds no A::h, so only B::h from ordinary lookup is used
    }
}
```
## 依赖型模板名称
通常而言， 编译器会把模板名称后面的<当做模板参数列表的开始，否则，<就是比较运算符。当引用的模板名称是 Dependent Name 时，编译器不会假定它是一个模板名称，除非显示的使用 template 关键字来指明，模板代码中常见的->template、.template、::template就应用于这种场景中。
```
template<unsigned long N>
void printBitset (std::bitset<N> const& bs) {
    std::cout << bs.template to_string<char, std::char_traits<char>, std::allocator<char>>();
}
```
这里，参数 bs 依赖于模板参数 N。所以，我们必须通过 template 关键字让编译器知道 bs 是一个模板名称

## 模板的模板参数匹配deque、vector
```
// error
template <typename T, template <typename> class Cont = std::deque>
class Stack {
  ...
};
```
std::deque和Cont不匹配。标准库的std::deque有两个参数，还有一个默认参数 Allocator。
1. 将 Cont 和 std::deque 的参数匹配即可
2. 可变参数模板
```
template <typename T,
          template <typename......>
          class Cont = std::deque>
class Stack {
......
};
```
但是，这点对于std::array无效，因为 std::array 的第二个参数是非类型模板参数 