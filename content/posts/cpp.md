---
title: cpp
date: 2021-05-28
---

## 模板基类Policy的析构函数
> c++设计新思维 1.7 Policy Class的析构函数

基类指针指向派生类实例，并delete该指针。如：
```
typedef WidgetManager<PrototypeCreator>
    MyWidgetManager;
...
MyWidgetManager wm;
PrototypeCreator<Widget>* pCreator = &wm;
delete pCreator;
```
* 定义虚析构函数。但是会妨碍policy的静态连结特性，影响执行效率；给对象大小带来额外开销。应该避免；
* protected或private继承。失去丰富的policies特性；

解决方法：定义non-virtual protected析构函数。只有派生类才可以delete这个指针

## protected 或 private析构函数
从语法上来讲，一个函数被声明为protected或者private，那么这个函数就不能从外部直接被调用了。protected的函数，可以被子类的内部函数调用。private的函数，只能被本类内部函数调用。

因此，protected析构函数可用于禁止直接实例化该类，不影响其子类；限制栈对象生成。

析构函数声明为private，该类不能被继承。

## 为什么c++没有反射
> c++设计新思维 8.2 Object Factories in C++: Classes和Object

在C++中classes和object是不同的东西。classes由程序员产生，objects由程序产生。无法在执行期产生新的classes，也无法在编译期产生objects。你无法拷贝一个class，将它保存于变量中，或是从某个函数中返回。

但是在某些语言中，classes就是objects。在那些语言中，具有某些属性的objects习惯上会被视为classes。因此，在那些语言中，你可以于执行期产生新的classes，拷贝一个classes，将它保存于变量等等。

## friend
友元保护了封装性。如果没有friend你就得把需要访问的元素设成public了

主要用于主类+辅助类这种设计方式。辅助类需要可以访问主类的私有成员。在这里辅助类实际逻辑上是主类的一部分，但是物理上分开更好，常见的辅助类比如迭代器，守护器等等。

friend operator大多数情况下就是起着成员函数的作用，只不过因为操作数顺序问题没搞成成员函数。

## 头文件重复包含与名字重定义
`#pragma once`解决同一个编译单元重复包含,但无法解决不同编译单元重复定义的。在不同编译单元定义多个同名函数时，会链接出错；而定义多个同名类则不会报错：根据链接顺序选择其中一个。定义不同实现的同名类是ub。

可以用匿名命名空间或static解决重定义问题。使用非类型模板参数时，只能是整型，包括enum，或者是指向外部链接对象的指针。而匿名命名空间的成员是有外部链接的，而static声明没有外部链接。因此：
```
template <void fn()>
class Foobar {};

namespace {
   void func1() {};
}

static void func2() {};

int main() {
    Foobar<func1> a; // ok
    Foobar<func2> b; // error
}
```

链接属性，即linkage，表明一个符号的可见性范围。C++继承了C的链接属性，分为三种:
* no linkage，即符号只允许在作用域内可见，其他不可见。例如，函数内部的局部变量就没有链接属性，只能在本函数内使用，超出函数范围则自动销毁。
* internal linkage，符号可以在其作用域内可见，也可以在同一个tu内可见。例如，静态全局变量就具有internal linkage，在本tu中可见，但在其他tu中不可见。静态函数同样具有internal linkage。
* external linkage，符号在作用域内可见，在同一个tu内可见，在其他tu内也可见。非静态的全局变量，以及非静态函数具有external linkage。

由于字符串文字是内部链接对象（因为两个具有相同名称但处于不同模块的字符串，是两个完全不同的对象），所以你不能使用它们来作为实参
```
template <char const* name>
class MyClass {
...
};
MyClass<"hell"> x;   //ERROR
```
另外，你也不能使用全局指针作为模版参数:
```
template <char const* name>
class MyClass {
...
};

char const* s = "hello";
MyClass<s> x;         //s是一个指向内部链接对象的指针
```
> 以上c++11后可以了

## include+宏的小技巧

There is also the X Macro idiom which can be useful for DRY and simple code generation :

One defines in a header gen.x a kind of table using a not yet defined macro :
```
/** 1st arg is type , 2nd is field name , 3rd is initial value , 4th is help */
GENX( int , "y" , 1 , "number of ..." );
GENX( float , "z" , 6.3 , "this value sets ..." );
GENX( std::string , "name" , "myname" , "name of ..." );
```
Then he can use it in different places defining it for each #include with a usually different definition :
```
class X
{
public :

     void setDefaults()
     {
#define GENX( type , member , value , help )\
         member = value ;
#include "gen.x"
#undef GENX
     }

     void help( std::ostream & o )
     {
#define GENX( type , member , value , help )\
          o << #member << " : " << help << '\n' ;
#include "gen.x"
#undef GENX
     }

private :

#define GENX( type , member , value , help )\
     type member ;
#include "gen.x"
#undef GENX
}
```


## 单例模式的Double-Checked Locking Pattern
```
class Singleton {
public:
  static Singleton& GetInstance() {
    if (!instance_) {
      std::lock_guard<std::mutex> lock(mutex_);
      if (!instance_) {
        instance_.reset(new Singleton);
      }
    }
    return *instance_;
  }

  ~Singleton() = default;

private:
  Singleton() = default;

  Singleton(const Singleton&) = delete;
  Singleton& operator=(const Singleton&) = delete;

private:
  static std::unique_ptr<Singleton> instance_;
  static std::mutex mutex_;
};
```
> https://blog.csdn.net/tantexian/article/details/50684689

事实上DCLP的方法也不能解决多线程环境的共享资源保护问题，其主要原因是 instance_.reset(new Singleton) 并非原子操作，编译器会将其转换成三条语句来实现：
1. 为Singleton对象分配一片内存
2. 构造一个Singleton对象，存入已分配的内存区
3. 将pInstance指向这片内存区

编译器有时会交换步骤2和步骤3的执行顺序，导致线程不安全。

C/C++的编译器和链接器执行这些优化操作时，只会受到C/C++标准文档中定义的抽象机器上可见行为的原则这唯一的限制。

C++抽象机器上的可见行为包括：volatile数据的读写顺序，以及对输入输出(I/O)库函数的调用。将声明成volatile的数据作为左值来访问对象，修改对象，调用输入输出库函数，抑或调用其他有以上相似操作的函数，都会产生副作用（side effects），即执行环境状态发生的改变。

有一点很重要：这些抽象机器默认是单线程的。C/C++作为一种语言，二者都不存在线程这一概念，因此编译器在优化过程中无需考虑是否会破坏多线程程序。

既然如此，程序员怎样才能用C/C++写出能正常工作的多线程程序呢？通过使用操作系统特定的库来解决。例如Posix线程库(pthreads)，这些线程库为各种同步原语的执行语义提供了严格的规范。由于编译器生成代码时需要依赖这些线程库，因此编译器不得不按线程库所约束的执行顺序生成代码。这也是为什么多线程库有一部分需要用直接用汇编语言实现，或者调用由汇编实现的系统调用（或者使用一些不可移植的语言）。换句话说，你必须跳出标准C/C++语言在你的多线程程序中实现这种执行顺序的约束。DCLP试图只使用一种语言来达到目的，所以DCLP不可靠。


---

C++11 保证静态局部变量的初始化过程是线程安全的。
* 第一次执行到变量声明的地方时才进行初始化。
* 初始化过程中发生异常的话视为未完成初始化，下次有代码执行到相同位置时再次初始化。
* 在当前线程执行到需要初始化变量的地方时，如果有其他线程正在初始化该变量，则阻塞当前线程，直到初始化完成为止。
* 如果初始化过程中发生了对初始化的递归调用，则视为未定义行为。

在C++11中提供另一种方法，使得函数可以线程安全的只调用一次。即使用std::call_once和std::once_flag：
```
class Singleton {
public:
  static Singleton& GetInstance() {
    static std::once_flag s_flag;
    std::call_once(s_flag, [&]() {
      instance_.reset(new Singleton);
    });

    return *instance_;
  }

  ~Singleton() = default;

private:
  Singleton() = default;

  Singleton(const Singleton&) = delete;
  Singleton& operator=(const Singleton&) = delete;

private:
  static std::unique_ptr<Singleton> instance_;
};
```
## 条件变量 cv.wait  
> https://zhuanlan.zhihu.com/p/77999255

主线程修改ready后，通过cv.notify_one发出通知，然后通过cv.wait等待processed变量被置true。注意wait的写法。wait有两个重载函数，第一个只接受unique_lock，第二个还接受Predicate。
```
void wait( std::unique_lock<std::mutex>& lock );
template< class Predicate >
void wait( std::unique_lock<std::mutex>& lock, Predicate pred );
```
如果你以为Predicate版本的wait函数等价于如下的if判断，那你就错了。为了处理小概率的虚假唤醒，该版本的wait其实等价于while的实现版本：
```
if(!pred())
  wait(lock);

while(!pred())
  wait(lock);
```

## 为什么C++11引入了std::ref
函数模板 ref 与 cref 是生成 std::reference_wrapper 类型对象的帮助函数，它们用模板实参推导确定结果的模板实参。所以std::ref()返回的实际上是一个reference_wrapper而不是T&，可以从一个指向不能拷贝的类型的对象的引用生成一个可拷贝的对象。

std::reference_wrapper 的实例是对象（它们可被复制或存储于容器），但它们能隐式转换成 T& ，故能以之为以引用接收底层类型的函数的参数。

主要是考虑函数式编程（如std::bind）在使用时，bind()不知道生成的函数执行的时候，传递进来的参数是否还有效。所以它选择参数值传递而不是引用传递。
```
#include <functional>
#include <iostream>
 
void f(int& n1, int& n2, const int& n3)
{
    std::cout << "In function: " << n1 << ' ' << n2 << ' ' << n3 << '\n';
    ++n1; // increments the copy of n1 stored in the function object
    ++n2; // increments the main()'s n2
    // ++n3; // compile error
}
 
int main()
{
    int n1 = 1, n2 = 2, n3 = 3;
    std::function<void()> bound_f = std::bind(f, n1, std::ref(n2), std::cref(n3));
    n1 = 10;
    n2 = 11;
    n3 = 12;
    std::cout << "Before function: " << n1 << ' ' << n2 << ' ' << n3 << '\n';
    bound_f();
    std::cout << "After function: " << n1 << ' ' << n2 << ' ' << n3 << '\n';
}
```
```
Output:
Before function: 10 11 12
In function: 1 11 12
After function: 10 12 12
```


## 为什么每个编译器非得自己写一个标准库

c++标准库里的许多模板函数，是没法用c++代码写出来的。它自己的标准库（stl）不符合它自己的语法规范。你自己写代码山寨，是山寨不出这些函数的。只能把代码写在编译器里，让编译时用黑魔法来实现。所以做编译器时，就必须自己也做一个标准库，实现这些没法自举的模板函数。

当然，也有的编译器没自己写标准库，而是用操作系统原生编译器的库，但那样也很麻烦，得一个个系统手动适配对方的的黑魔法，识别出对方的这些没法自举的函数最后把调用转发到什么鬼地方，然后自己在编译时把它填好，而不是报未定义错误。

所以这些模板函数，在头文件里只看得到声明，看不到实现。在语言标准里，模板函数必须开放实现，不允许只有个声明。

以 VC 标准库 msvcrt 里的 std::is_union 为例：
```
// STRUCT TEMPLATE is_union
// determine whether _Ty is a union
template <class _Ty>
struct is_union : bool_constant<__is_union(_Ty)> {}; 

template <class _Ty>
_INLINE_VAR constexpr bool is_union_v = __is_union(_Ty);

```
这个模板函数是没法用 c++ 语法手动写出来的，也就是没法自举，所以标准库里就只是把它的调用转发到了一个根本连声明都追溯不到的 __is_union 里，相当于额外扩展了一个叫做 __is_union 的关键字，然后编译器把这个关键字用黑魔法实现，假装自己没有添加关键字，而是写的模板。

如果某个第三方编译器想用 msvcrt 而不是自己写，那么就得自己用黑魔法实现 __is_union ，避免编译时报错。而且还得紧跟 VC 更新，免得它某个版本不叫 __is_union 了于是又编译不对了。

> boost loki 的文档：Without (some as yet unspecified) help from the compiler, we cannot distinguish between union and class types using only standard C++, as a result this type will never inherit from true_type

C++之父在设计和演化里说优先库函数，实在不行再加语言特性。很多分明应当作为语言builtin的东西，标准里面非得按模板去凑。最后的实现还是编译器自己扩展加了关键字，但非要装作是个函数而不是语言关键字。

## libstdc++ 与 libc++
libstdc++是gcc搞的，libc++是llvm搞的，他们都是C++标准库的实现。

## vector 与 array
vector和array的选择显然不是“在乎性能”。因为两个容器均能随机访问。如果需要的大小在运行时创建容器的时候就知道（不需要动态扩张），默认初始化vector然后reserve就行了，也不需要copy/move元素。

vector和array的下标都没有检查越界的，要检查越界用`.at()`，你要设计一个新容器，下标也不应该检查越界抛异常，而是应该实现`.at()`以符合标准库的风格，不给你的库用户制造surprise。

## undefined behavior
程序行为不应该是由特定实现定义的,而是应该由接口描述定义的。

比如某个函数的描述是：输入0返回1。

这个描述其实可以理解成在任意状态(因为接口描述中没说在某个特定状态下,所以就可以理解为在任意状态下)下除了输入0的行为是定义的，在任意状态下其他输入的行为都是未定义的。

很显然未定义行为和什么程序语言，和是否崩溃，和是否抛异常是毫无关系的。更准确的说未定义行为和实现是毫无关系的，未定义行为指的是接口描述中所有未说明的情况的行为都是未定义行为。

把包含未定义行为的接口变成不包含未定义行的接口为的唯一方法是修改接口描述，这样这个函数在任意状态下的整个参数类型的自然定义域内就没有未定义行为了。

所以C/C++在研究它的特性的时候永远不能基于运行期的事实现象，而是应该基于文档和标准来编程。因为UB不是每次都会让你吃惊的，而且有时候因为程序小所以UB不会让你疼。

谁能彻底消灭UB，谁就能解决停机问题。

---

>  Algorithms that take a single iterator denoting a second sequence assume that the second sequence is at least as large at the first

以下代码是不正确/不规范的，因为存在越界问题：

```
vector<int> vec1 = {1, 2};
vector<int> vec2 = {1};
equal(vec1.cbegin(), vec1.cend(), vec2.cbegin());
```
标准里在 equal 条目里有写 last2 不提供的版本使用 first2 + (last1 - first1)

## 库
在链接过程中，如果某个目标文件中的符号被用到了，那么这个目标文件会单独从库文件中提取出来并入可执行程序，而其余的目标文件则会被丢弃。因此在链接静态库的时候，应该将被依赖的库放在最右边。

在windows上，默认情况下编译的dll是不导出符号的，所以要手动添加dllexport。

但在*nix下，默认编译的so是会导出所有符号的，所以大家就不用考虑区分静态库/动态库，import/export，很是方便。不过这样带来的弊病就是，遇上重复的符号时，你也不知道编译器会选择哪一个，于是就存在冲突或者bug的可能。

`-fvisibility=hidden`和`__attribute__((visibility("default")))`的使用

## class template argument deduction
看上去只是 function template argument deduction 的延伸，但实际上它夹带的 deduction guide 特性，才是 main feature。

如果给这个特性换个名字，叫 specialization selector, 你就明白了。比如你有这么一个模板类，
```
template <typename Queue, typename F>
class Tasks
{
    Tasks(Queue&& q, F&& on_finished);
};
```
本意是允许用户自己定制 Callback 的类型，结果用户写`Tasks(v, []{ cout << ...; });`
得到一堆各不相同的 `Tasks<T, lambda>`, 放不进同质容器。

但只要有这样一个 guidetemplate 
```
<typename Queue, typename Fn>
Tasks(Queue, Fn) -> Tasks<Queue, std::function<void()>>;
```
用户写上面的代码时就会选择到 `Tasks<Queue, std::function<void()>>` 这个 specialization, 然后尝试用 lambda 初始化`std::function<void()>`, 达成「用户可以自己指定 Callback 类型，默认使用 type erasure 」，给 API 增加很多弹性。


## tie 和 structured bingdings
Structured bindings has specific language rules to handle arrays and certain other types. tie() is specifically a tuple<T&...> and can only be assigned from another tuple<U&...>.


## vector<bool>
`vector<bool>` 有两个问题．
* 它不是一个真正 STL 容器
* 不保存 bool 类型．

一个东西要成为STL容器，必须满足所有列于C++标准23.1节的容器要求。在这些要求中，有这样一条：如果C是一个T类型元素容器，并且C支持operator[]。那么以下代码必须能够编译:
```
T *p = &c[0];   // initialize a T* with the address
                // of whatever operator[] returns
```
标准库提供了两个替代品，它们满足几乎所有的需求:
    
* `deque<bool>`  deque提供了几乎多有vector所提供的，而且`deque<bool>`保存真正的bool值
    
* `bitset`　bitset 不是STL容器，是C++标准库的一部分，大小在编译期固定，因此不支持插入和删除元素，不是迭代器，不支持iterator。压缩表示，每个值只占用一比特。提供vector<bool> 特有的 flip 成员函数，还有一些列其他操作位集所特有的成员函数。如果不在意没有迭代器和动态改变大小，bitset正合适。

## 迭代器失效
vector迭代器失效问题总结:
* 当执行erase时，指向删除节点的迭代器全部失效，指向删除节点之后的全部迭代器也失效
* 当进行push_back时，end操作返回的迭代器肯定失效。
* 当push_back一个元素后，capacity返回值与没有插入元素之前相比有改变，则需要重新加载整个容器，此时first和end操作返回的迭代器都会失效。
* 当push_back一个元素后，如果空间未重新分配，指向插入位置之前的元素的迭代器仍然有效，但指向插入位置之后元素的迭代器全部失效。

vector的erase操作可以返回下一个有效的迭代器，所以每次执行删除操作的时候，将下一个有效迭代器返回

deque迭代器失效总结：
* 对于deque,插入到除首尾位置之外的任何位置都会导致迭代器、指针和引用都会失效，但是如果在首尾位置添加元素，迭代器会失效，但是指针和引用不会失效
* 如果在首尾之外的任何位置删除元素，那么指向被删除元素外其他元素的迭代器全部失效
* 在其首部或尾部删除元素则只会使指向被删除元素的迭代器失效。

对于关联容器(如map, set,multimap,multiset)，删除当前的iterator，仅仅会使当前的iterator失效，只要在erase时，递增当前iterator即可。

## static_assert(false)
> https://zhuanlan.zhihu.com/p/371769440

直接static_assert(false)是不可以的，因为编译器解析这个语句时就会报错，即使分支不会走到。修改如下：
```
template <typename> constexpr bool false_c = false; 

template<typename T>
void f()
{
    if constexpr (false)
        static_assert(false_c<T>);

    if constexpr (false)
        []<bool flag = false>()
            {static_assert(flag, "no match");}();
}

```
v8中的用法：
```
template <typename T>
struct TypeInfoHelper {
    static_assert(sizeof(T) != sizeof(T), "This type is not supported");
};

#define SPECIALIZE_GET_TYPE_INFO_HELPER_FOR(T, Enum)                              \
    template <>                                                                   \
    struct TypeInfoHelper<T> {                                                    \
        static constexpr CTypeInfo::Flags Flags() {                               \
            return CTypeInfo::Flags::kNone;                                       \
        }                                                                         \
                                                                                  \
        static constexpr CTypeInfo::Type Type() { return CTypeInfo::Type::Enum; } \
    };

#define BASIC_C_TYPES(V)        \
    V(void, kVoid)              \
    V(bool, kBool)              \
    V(int32_t, kInt32)          \
    V(uint32_t, kUint32)        \
    V(int64_t, kInt64)          \
    V(uint64_t, kUint64)        \
    V(float, kFloat32)          \
    V(double, kFloat64)         \
    V(ApiObject, kApiObject)    \
    V(v8::Local<v8::Value>, kV8Value)

BASIC_C_TYPES(SPECIALIZE_GET_TYPE_INFO_HELPER_FOR)
```

## 侵入式容器
已知struct type和type的成员变量field，可以得到field在A中的偏移：
```
((ptrdiff_t)(&((type *)0)->field))`
```
若有一个指向某type实例中field成员的指针，可以得到该type势力：
```
((type *)((void *)(ptr) - offsetof(type, field))))`
```
侵入式容器中，链表节点不需要存储数据结构体，而是让数据结构体存储链表节点。一个对象能同时存在于多个链表中，只需要存储多个节点。节点的生命周期独立于容器而存在。

linux内核的链表是侵入式（intrusive），就是和一般的数据结构书上教的差不多，每个节点本身包含了next/prev指针。而STL的链表属于非侵入式（non-intrusive），你可以放里面放任何类型，不需要含有next/prev指针。

标准库中list帮你维护对象的生命周期，自己不需要维护前向和后向指针，标准库全部帮你搞定。侵入式的链表，对象的生命周期需要自己管理——如果对象被释放，但链表里还挂着这个对象，那随后的崩溃几乎就不可避免的了。

侵入式链表的主要好处就在于你需要把链表结点挂到多个容器里的时候，删除结点时你需要同时从多个容器里删除。一种可能的场景是你使用一个 std::map 来缓存某个计算的结果，同时用一个按最近使用时间排序（LRU）的链表来决定从哪儿开始丢弃最老的结果。这种情况下，使用 std::list 一般意味着你需要放对象的指针或智能指针到链表里，这样，在链表遍历时就会多一次间接，同时链表里的每一项也意味着额外的内存分配。你很可能也做不到彻底的不侵入，而必须在对象里放上一个 list::iterator 来允许今后从容器里释放自己。

uc/os里面调度代码中，调度队列里面保存的是每个pcb中一个成员变量的地址，通过这种方式根据成员变量地址获取结构体首地址。

## 函数的实现放在头文件中，避免重复定义的链接错误方法
* inline关键词声明的函数容许跨编译单元有重复定义，但是使用到的编译单元必须有定义，这些重复定义必须是等价的。
* static或匿名命名空间声明的函数在不同编译单元间表现为不等价的函数，若内部有静态局部变量则它们是不同的变量。
* 模板函数与inline类似，唯一的不同在于模板函数允许某个使用到的编译单元没有定义，仅有前置声明。

注意msvc允许inline函数仅有前置声明，而Clang遵循标准规定会报出链接错误。

## static在c++中的用法
静态成员变量、静态成员函数、静态局部变量、内部链接的函数

## string_view
使用string_view要确保引用的字符串生命周期比string_view长

## noexcept
在C++ 11中类结构隐式自动声明的或者是由程序员主动声明的不带有任何修饰符的析构函数，都会被编译器默认带上noexcept (true)标记，以表示这个析构函数不会抛出异常。

如果一个类的父类析构函数或者它的成员函数被标记为了可抛出异常，那么这个类的析构函数就会默认被标记为可抛出异常，也就我们所说的受到了污染。

STL为了保证容器类型的内存安全，在大多数情况下只会调用被标记为不会抛出异常的移动构造函数，否则会调用其拷贝构造函数来作为替代。所以move constructor/assignment operator 如果不会抛出异常，一定用noexcept。

当vector中的元素的移动拷贝构造函数是noexcept时，vector就不会使用copy方式，而是使用move方式将旧容器的元素放到新容器中：

