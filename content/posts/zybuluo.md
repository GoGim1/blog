---
title:  作业部落Cpp
date: 2021-05-28
---


## Lambda表达式直接捕获this指针
## 继承类模板后，用this调用基类成员（模板二次编译）
## 模板链式继承
## -L 和 LD_LIBRARY_PATH 的区别 

## this 指针

> https://zhuanlan.zhihu.com/p/93752913

C++ 内部有一个隐藏的 this 指针，指向当前的类对象。上面的 lock() 方法其实有一个隐藏的参数，它的真身如下：
```
void lock(Mutex* this) {
    pthread_mutex_lock(&this->_mutex);
}
```
通过 mutex.lock() 调用 lock() 方法时，C++ 编译器默默的将 mutex 的地址传给了 lock()。另外指出，this 是 C++ 中的一个关键字，可以在类的函数成员中显示使用它。

## 类的访问修饰符(Access Modifiers)

* public 成员在类外、类中都可以访问
* private 成员只允许本类中的函数成员访问
* protected 成员在类或子类中可以访问，但不能从类外访问
    
## 继承

* public 继承最常用，基类成员的访问权限在子类中保持不变。
* protected 继承很少用，基类的 public 成员在子类中会变成 protected 成员。
* private 继承偶尔会用到，基类的所有成员在子类中都变成 private 成员。

## 不能通过 mutex.Mutex() 手动调用构造函数
原因在于构造函数在内部是匿名(没有名字)的，Mutex 类中根本就不存在名字为 Mutex 的函数。然而，C++ 允许调用析构函数，像下面这样：
```
mutex.~Mutex();
```
建议不要这么做，它会导致析构函数被调用两次，程序很可能因此崩溃。

## 动态创建对象

> 不建议这么做

如果需要动态创建对象的数组，可以用 new[] 与 delete[] 操作符，代码如下：
```
T* t = new T[3];
delete[] t;
```
但是有一个限制，没办法调用类的带参数的构造函数，如下的代码是错误的：
```
T* t = new T[3](2);
T* t = new T(2)[3];
```
C++11 中提供了一个解决办法：
```
T* t = new T[3] {2, 2, 2};
```

## name mangling

由于 c 语言和 c++ 的 name mangling 方式是不同的，所以就会产生一个问题：如果在 c++ 语言中调用 c 的函数，就会因为在链接时找不到函数而产生错误。解决的办法就是在用到的 c++ 函数之前加上关键字 **extern "C"**，这样 c++ 的函数就会按照 c 语言的 name mangling 方式，链接便能正确执行。

c++filt指令 ，看name mangling后的函数名

## Unevaluated Operands
> https://blog.csdn.net/IndignantAngel/article/details/44015361

C++98标准中的Unevaluated Operands，只有sizeof操作符。C++11又引入了decltype，typeid和noexcept。Unevaluated Operands不会有求值的计算，即使是在编译期间。这意味着Unevaluated Operands 中的表达式甚至不会生成具体的C++代码，操作符中的表达式仅需要声明而无需定义。在模板元编程中，我们有时候经常仅需要对catch a glimpse of一个表达式，而这些操作符都是很好的工具。
```
namespace cpp11 {	
    template <typename T>	
    struct is_copy_assignable {	
    private:		
        template <typename U, typename = decltype(std::declval<T&>() = std::declval<T const&>())>	
        static std::true_type try_assign(U&&); 		

        static std::false_type try_assign(...); 	
    public:		
        using type = decltype(try_assign(std::declval<T>()));	
    };
}

```
```
namespace cpp98
{
	namespace detail
	{
		template <typename T>
		T declval();
	}

	template <typename T>
	struct is_copy_assignable
	{
	private:
		typedef char One;
		typedef struct { char a[2]; } Two;

		template <int N = sizeof(detail::declval<T&>() = detail::declval<T const&>())>
		static One try_assign(int);

		static Two try_assign(...);

	public:
	typedef typename std::conditional<sizeof(try_assign(0)) == sizeof(One),
			std::true_type, std::false_type>::type type;
	};
}
```
```
namespace cpp1y
{
	template <typename ... Args>
	struct make_void 
	{
		typedef void type;
	};
	
	template <typename ... Args>
	using void_t = typename make_void<Args...>::type;
 
	template <typename T, typename = void>
	struct is_copy_assignable : std::false_type
	{};
 
	template <typename T>
	struct is_copy_assignable<T, void_t<decltype(std::declval<T&>() = std::declval<T const&>())>> : std::true_type
	{};
}
```
## argument-dependent lookup(ADL)
> https://blog.csdn.net/qq_17550379/article/details/80007769

当我们给函数传递一个类类型的对象时，首先会在常规的作用域查找，其次在实参类所属的命名空间查找。
查找顺序如下：
1. 先在本作用域内查找；
2. 在实参的命名空间 和 全局作用域中 同时查找；

最初ADL引入的动机只是为了方便编写a+b，其中a和b至少有一个是用户自定义类型。如果没有ADL，则就要写为N::operator+(a,b)。
```
namespace c1
{
    namespace c2
    {
        struct cc{};
        void f(cc& o){}             //#1
    }
}
void f(c1::c2::cc& o){}
namespace f1
{
    namespace f2
    {
        void f(const c1::c2::cc& o){}   //#2
        void g()
        {
            c1::c2::cc o;
            const c1::c2::cc c(o);
            f(o);
            f(c);
        }
        void f(c1::c2::cc& o){}     //#3
    }
}
```
因为#3是定义于g的后面，所以在g中是不可见的。全局函数::f被#2隐藏（name hiding）。因此对于f(o)来说，我们通过使用ADL可以调用#1，我们通过name hiding也可以调用#2，但是我们最后调用最佳匹配#1。而对于f(c)我们通过同样的分析，我们知道调用#2。

## 运用ADL扩展模板库
> https://blog.csdn.net/IndignantAngel/article/details/70207260

C++的语言规范不允许在一个命名空间中特化另一个命名空间的类模板，也不允许在一个命名空间下特化全局命名空间的类模板。
```
// 这里是你的库类中需要扩展的组件
namespace lib
{
    void to_extend(...) {}
}

// 客户处的扩展代码
namespace client
{
    struct foo {};

    // 下面的代码通常会被宏来生成
    auto to_extend(foo const&)
    {
        struct foo_extend
        {
            static constexpr char const* name() { return "foo"; }
        };

        return foo_extend{};
    }
}

// 在库中，可能是这样集成组件的
namespace lib
{
    constexpr char const* apply_impl(std::true_type, ...)
    {
        return "null";
    }

    template <typename T>
    constexpr char const* apply_impl(std::false_type, T)
    {
        return T::name();
    }

    template <typename T>
    constexpr char const* apply(T const& t)
    {
        using type = decltype(to_extend(t));        // 探测有没有对应的to_extend函数，ADL查找保证
        using is_void = std::is_same<type, void>;
        return apply_impl(is_void{}, type{});
    }
}

// 在client中可能是这样使用的
namespace client
{
    void some_function()
    {
        foo f;
        std::cout << lib::apply(f) << std::endl;
    }
}
```
简单解释一下，下面的代码是如何工作的。首先，函数的 ADL 查找，是 apply 函数尝试调用 to_extend 的时候，不仅会查找 lib 命名空间下的符号，也会去查找 T 类型所在命名空间的符号。我们定义的 to_extend 函数在 client 命名空间下，foo 类型也在 client 命名空间下。那么 ADL 查找肯定可以找到 to_extend 函数的符号。然后，我们没有选择类模板的特化，而是选择了使用 to_extend 函数，返回一个它内部定义的类型作为 policy 的功能。

## 模板静态成员的定义及实例化
> https://www.cnblogs.com/tangzhenqiang/p/4332801.html

```
template <typename T> class Test{
public:
    static std::string info;
};

template <> string Test<int>::info("123");          //ok
template <typename T> string Test<T>::info("123");  //ok
template <typename T> string Test<T>::info;         //ok
template <> string Test<int>::info;                 //error，编译器认为它是一个声明而非定义。
template <> string Test<int>::info();               //error，声明一个函数
template <typename T> string Test<T>::info();       //error，声明一个函数
```
一般为了避免无法编译，应当尽量减少使用如下方式的定义
`template <typename T> string Test<T>::info;`
只有在你首次需要使用时在实现文件中给出如下特化定义即可，其他文件只要包含头文件就能使用。
`template <> string Test<int>::info("123");`

## 静态变量初始化
静态变量存储在虚拟地址空间的数据段和bss段，C语言中其在代码执行之前初始化，属于编译期初始化。而C++中由于引入对象，对象生成必须调用构造函数，因此C++规定全局或局部静态对象当且仅当对象首次用到时进行构造 

《C++设计新思维》说，使用编译期常量加以初始化的 static 变量`static int i = 100;`是在程序装载时初始化的。

----------

> https://www.cnblogs.com/wanyuanchun/p/4041080.html
在类内部初始化静态成员`class A { static int i = 1; };`必须为**字面值常量**类型的 **constexpr** (常量表达式) 

*不符合条件的只能类外初始化？*
```
static vector<double> vec(vecSize);  // error，不是字面值
static double rate = 6.5;   // error，不是常量
constexpr static const double rate = 6.5  // ok
```

## C++语言的15个晦涩特性
> https://www.cnblogs.com/lanxuezaipiao/p/3501078.html
http://madebyevan.com/obscure-cpp-features/

----------

ptr[3]其实只是*(ptr + 3)的缩写，与用*(3 + ptr)是等价的，因此反过来3[ptr]也合法。

----------

```
// 1)通过int(x)实例化的类型int变量，int y = 3;等价于int(y) = 3;
// 2)一个函数声明，返回一个int值并有一个参数，参数是一个名为x的int型变量
int bar(int(x));
```
C++标准要求的是第二种解释，即使第一种解释看起来更直观。程序员可以通过包围括号中变量的初始值来消除歧义`int bar((int(x)));`

**TODO 待测试**

----------

> 标记符and, and_eq, bitand, bitor, compl, not, not_eq, or, or_eq, xor, xor_eq, <%, %>, <: 和 :>都可以用来代替我们常用的&&, &=, &, |, ~, !, !=, ||, |=, ^, ^=, {, }, [ 和 ]。在键盘上缺乏必要的符号时你可以使用这些运算标记符来代替。

----------
C++11允许成员函数在对象的值类型上进行重载，依据 this 对象是左值还是右值选择重载：
```
struct Foo {
  void foo() & { std::cout << "lvalue" << std::endl; }
  void foo() && { std::cout << "rvalue" << std::endl; }
};
 
int main() {
  Foo foo;
  foo.foo(); // Prints "lvalue"
  Foo().foo(); // Prints "rvalue"
}
```

----------

指向成员的指针
```
struct Test {
  int num;
  void func() {}
};

int Test::*ptr_num = &Test::num;
void (Test::*ptr_func)() = &Test::func;

Test t;
Test *pt = new Test;
(t.*ptr_func)();
(pt->*ptr_func)();
t.*ptr_num = 1;
pt->*ptr_num = 2;
```
成员函数指针与普通函数指针是不同的，在成员函数指针和普通函数指针之间casting是无效的。
thiscall 
**TODO**

----------

模板参数可以是函数
```
template <int (*F)(int)>
int memoize(int x) {
  return F(x);
}
```

## inline 
> https://blog.csdn.net/Hello_World_156_5634/article/details/90300356

----------

> https://www.zhihu.com/question/40793741
static function。你可以把一个函数标记为static（也称为internal linkage），这样该函数的symbol就会被隐藏，从而该函数只存在在当前translation unit。换了下一个translation unit之后，该函数被忘得一干二净。linker也从来不知道这函数存在过。这时候你就算再定义一次上次translation已经定义过的static函数，linker也不会报redefinition错误。当然，这样代码在binary中就出现了多次。

> 内联优化有个缺陷，就是在同一个translation unit里一定要看到函数体，所以光看到declaration是没用的。现在考虑这么个问题：传统的在头文件中声明，在一个文件(.c)中实现函数体的方式有时执行太慢了。为什么慢呢，假设我这个函数就一行，但是函数调用的压栈传参数弹栈跳转等指令占了大部分开销，真是太不合算了。
这时候在传统C里面有两个解决方案：
1) “宏函数”。就是把本来藏在.c文件里的函数体放到一个宏里面去，当然宏也在头文件里。然后大家include头文件的时候就把宏也include走了，使用宏的时候就把这段代码一遍遍展开到所有使用的地方，消除了函数调用的开销。
2) 在编译器支持内联优化的情况下，在头文件里定义static function。任何别的.c文件，只要include了你的头文件，都对你的头文件做了一次复制粘贴，自动获得了该static function的函数体。所以在不同的translation unit里面，这些函数并不冲突，因为它们是static的。值得一提的是，这些函数体不一定一模一样。

> inline关键字不仅编译器认识，而且编译器在没有真正内联该函数时，会通过某种方式提示linker说这个函数被标记为“可重复定义”耶 - 根据我用gcc的实验，生成的是一个weak symbol。当linker看到一个weak symbol，会把函数名写在一个小本本上。在linker最后把所有文件link到一起的时候，它会把小本本扫一遍，对于同名函数只留一个，别的函数连带函数体统统删掉。这样就解决了binary size bloat的问题。当然这只是一种典型实现方式，并非一定如此。
另外，在编译器真正内联了该函数的时候，效果就和static一样了，这也是为什么你的代码里找不到定义 - 因为linker根本看不到static函数。


## 调用约定
__cdecl是C和C＋＋程序的默认调用约定：参数通过堆栈来传递，从右向左依次入栈，由调用者平衡堆栈。

__stdcall的调用约定是参数通过堆栈来传递，从右向左依次入栈，由被调用者平衡堆栈。一般Windows API函数都是__stdcall。

__fastcall的调用约定是：第一个参数通过ECX传递，第二个参数通过EDX传递，第三个参数起从右向左依次入栈，由被调用者平衡堆栈。

注意入栈顺序不等于求值顺序。

## std::conditional
不同于常规的if-then-else语句，这里所有分支的模板实参在被选择前都会被计算，所以不能有非法的代码
```
// T是bool或非整型将产生未定义行为
template<typename T>
struct UnsignedT {
    using Type = std::conditional_t<std::is_integral_v<T> && !std::is_same_v<T, bool>,
        std::make_unsigned_t<T>, T>; // 无论是否被选择，所有分支都会被计算
};
```
添加一个类型函数作为中间层即可解决此问题
```
// yield T when using member Type:
template<typename T>
struct IdentityT {
    using Type = T;
};

// to make unsigned after IfThenElse was evaluated:
template<typename T>
struct MakeUnsignedT {
    using Type = std::make_unsigned_t<T>;
};

template<typename T>
struct UnsignedT {
    using Type = std::conditional_t<std::is_integral_v<T> && !std::is_same_v<T, bool>,
        MakeUnsignedT<T>, IdentityT<T>>;
};
```
别名模板`template<typename T>
using MakeUnsigned = typename MakeUnsignedT<T>::Type;`并不能有效地用于conditional的分支。
使用别名模板总会实例化类型，将使得对给定类型难以避免无意义的实例化traits。

## 用于类模板的标签分派
> https://downdemo.gitbook.io/cpp-templates-2ed/part3-mo-ban-yu-she-ji/17.-ji-yu-lei-xing-shu-xing-de-zhong-zai-overloading-on-type-property/lei-te-hua/yong-yu-lei-mo-ban-de-biao-qian-fen-pai

```
// construct a set of f() overloads for the types in Types...:
template<typename... Types>
struct A;

// basis case: nothing matched:
template<>
struct A<> {
    static void f(...); // 如果下面对所有参数尝试匹配失败，则匹配此版本
};

// recursive case: introduce a new f() overload:
template<typename T1, typename... Rest>
struct A<T1, Rest...> : public A<Rest...> { // 递归继承
    static T1 f(T1); // 尝试匹配第一个参数，如果匹配则返回类型T1就是所需Type
    using A<Rest...>::f; // 否则递归调用自身基类，尝试匹配下一个参数
};

// find the best f for T in Types...:
template<typename T, typename... Types>
struct BestMatchInSetT { // f的返回类型就是所需要的Type
    using Type = decltype(A<Types...>::f(std::declval<T>()));
};

template<typename T, typename... Types>
using BestMatchInSet = typename BestMatchInSetT<T, Types...>::Type;
```

## 空基类优化
确保两个不一样的对象拥有不同的地址，C++ 中空类对象占一个字节。

空类最常用于作为基类，那时候为了对齐实际可能占4个字节或以上。但这样会浪费空间，尤其是多重继承多个空基类的時候。所以编译器有空基类优化（empty base class optimization, EBCO），令无非静态数据成员、无虚函数的基类实际占0字节。

空基类只在没有歧义的情况下会优化，如果
`class A{}; class B:A { A a; };`那就没有空基类优化，因为如果优化的话，B类的基类A和B类的的成员a就有了相同的地址，这是不合理的。

## 结合Barton-Nackman Trick与CRTP的运算符实现
> https://downdemo.gitbook.io/cpp-templates-2ed/part3-mo-ban-yu-she-ji/18.-mo-ban-yu-ji-cheng-template-and-inheritance/qi-yi-di-gui-mo-ban-mo-shi-the-curiously-recurring-template-patterncrtp/jie-he-bartonnackman-trick-yu-crtp-de-yun-suan-fu-shi-xian

```
template<typename T>
class A {
public:
    friend bool operator!=(const T& x1, const T& x2)
    {
        return !(x1 == x2);
    }
};

class X : public A<X> {
public:
    friend bool operator==(const X& x1, const X& x2)
    {
        // implement logic for comparing two objects of type X
    }
};

int main()
{
    X x1, x2;
    if (x1 != x2) {}
}
```

## 内联函数和constexpr函数可以在程序中定义不止一次
对于某个给定的内联函数或者constexpr函数来说，它的多个定义必须完全一致。基于这个原因，内联函数和constexpr函数通常定义在头文件中”

能定义不止一次的好处是方便你放到头文件里，放到头文件里的好处是每个include这个头文件的.c文件都能看到函数体，看到函数体的好处是编译器可以内联。内联的好处是代码变快了。另外，所有函数体定义必须一模一样，不然出了问题概不负责。**constexpr自带inline属性**。

## 内存模型
> https://blog.csdn.net/pongba/article/details/1659952

现有的单线程内存模型没有对编译器做足够的限制，从而许多我们看上去应该是安全的多线程程序，由于编译器不知道（并且根据现行标准（C++03）的单线程模型，编译器也无需关心）多线程的存在，从而可能做出不违反标准，但能够破坏程序正确性的优化（这类优化一旦导致错误便极难调试，基本属于非查看生成的汇编代码不可的那种）。

## Observable Behavior
标准把Observable Behavior（可观察行为）定义为volatile变量的读写和I/O操作。原因也很简单，因为它们是Observable的。volatile变量可能对应于memory mapped I/O，所有I/O操作在外界都有可观察的效应，而所有内存内的操作都是不显山露水的，举个简单的例子：
```
int main()
{
int sum;
…
for(int i = 0; i < n ; ++i) sum += arr[i];
printf(“%d”, sum);
}
```
## Concept
> https://blog.csdn.net/pongba/article/details/1726031